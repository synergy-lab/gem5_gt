# README #
Last Updated: February 18, 2020

This is a Georgia Tech internal gem5 repository used by Tushar Krishna (http://tusharkrishna.ece.gatech.edu) for his Interconnection Networks course.
This is an old version of the repo (from Jan 2018) and is primarily used to build Garnet2.0 in a standalone manner.

## How to Build ##
scons build/Garnet_standalone/gem5.opt

## Example Run Command ##
See my_scripts/run_example.sh

## NoC Labs ##
http://tusharkrishna.ece.gatech.edu/teaching/icn_s20/

## More details ##
http://tusharkrishna.ece.gatech.edu/teaching/garnet_gt/

(Ignore the my_scripts/build_Garnet_standlone.sh command as that is specific to the RHEL setup at the GT ECE servers).
 
## Useful links: ##
http://www.gem5.org/Garnet_Synthetic_Traffic

http://www.gem5.org/Garnet2.0
